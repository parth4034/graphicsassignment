﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GraphicsAssignment;
using System.Drawing;

namespace UnitTestProject
{
    /// <summary>
    /// Test Gaphics Assignment Functionality
    /// </summary>
    
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Test Clear Command
        /// </summary>
        [TestMethod]
        public void TestClearCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics= Graphics.FromImage(outputBitMap);
            Canvas canvas = new Canvas(graphics);
            canvas.Command = "clear";
            Command obj = new Command(canvas);

        }

        /// <summary>
        /// Test MoveTo Command
        /// </summary>
        [TestMethod]
        public void TestMoveToCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics = Graphics.FromImage(outputBitMap);

            Canvas canvas = new Canvas(graphics);
            canvas.Command = "moveto";
            canvas.Parameters = new string[] { "100", "200" };

            Command obj = new Command(canvas);

            Assert.AreEqual(100, canvas.xPos, 0.01);
            Assert.AreEqual(200, canvas.yPos, 0.01);

        }

        /// <summary>
        /// Test DrawTo Command
        /// </summary>
        [TestMethod]
        public void TestDrawToCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics = Graphics.FromImage(outputBitMap);

            Canvas canvas = new Canvas(graphics);
            canvas.Command = "drawto";
            canvas.Parameters = new string[] { "50", "50" };

            Command obj = new Command(canvas);

            Assert.AreEqual(50, canvas.xPos, 0.01);
            Assert.AreEqual(50, canvas.yPos, 0.01);

        }


        /// <summary>
        /// Test Validation - Valid Command
        /// </summary>
        [TestMethod]
        public void TestValidCommand()
        {
            IsValidate isValidate = new IsValidate("circle 100");
            Assert.IsTrue(isValidate.IsValid);
        }


        /// <summary>
        /// Test Validation - InValid Command
        /// </summary>
        [TestMethod]
        public void TestInValidCommand()
        {
            IsValidate isValidate = new IsValidate("rect abc,100");
            Assert.IsFalse(isValidate.IsValid);
            Assert.AreEqual(isValidate.ErrorMessage, "Enter Command Parameter is Invalid");
        }

        /// <summary>
        /// Test Multiline Command  - For Valid Command
        /// </summary>
        [TestMethod]
        public void TestMultiLineValidCommand()
        {
            string[] multilineCommand = new string[] {"moveto 100,100","drawto 150,150","circle 100","rect 80,80" };

            IsValidate isValidate = new IsValidate(multilineCommand);

            Assert.IsTrue(isValidate.IsValid);
        }

        /// <summary>
        /// Test Multiline Command  - For InValid Command
        /// </summary>
        [TestMethod]
        public void TestMultiLineInValidCommand()
        {
            string[] multilineCommand = new string[] { "moveto 100", "drawto 150,150", "circle 100,abc", "rect 8080" };

            IsValidate isValidate = new IsValidate(multilineCommand);

            Assert.IsFalse(isValidate.IsValid);

            Assert.AreEqual(isValidate.ErrorMessage, "Enter Command Parameter is Invalid - 1\r\nEnter Command Parameter is Invalid - 3\r\nEnter Command Parameter is Invalid - 4\r\n");
        }




    }
}
