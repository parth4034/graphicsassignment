﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for To Draw Circle
    /// Have a Single Responsibility to Draw Circel Based on Paramter
    /// Used Execute Interface 
    /// </summary>
    public class Circle : IExecute
    {
        /// <summary>
        /// Constructor with Parameter as Canvas
        /// </summary>
        /// <param name="canvas"></param>
        public Circle(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Draw Circle
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            int radius = Convert.ToInt32(canvas.Parameters[0]);

            canvas.graphics.DrawEllipse(canvas.pen, new Rectangle(canvas.xPos - (radius / 2), canvas.yPos - (radius / 2), radius, radius));
            if (canvas.Fill.Equals("on"))
            {
                System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(canvas.pen.Color);
                canvas.graphics.FillEllipse(myBrush, new Rectangle(canvas.xPos - (radius / 2), canvas.yPos - (radius / 2), radius, radius));
            }
        }
    }
}
