﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for Clear Drawing Area
    /// Clear Command Execution
    /// </summary>
    public class Clear : IExecute
    {

        /// <summary>
        /// Constructor with Canvas as Parameter
        /// </summary>
        /// <param name="canvas"></param>
        public Clear(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Clear the Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            canvas.graphics.Clear(Color.LightGray);
        }

    }

}
