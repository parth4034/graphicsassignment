﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for Rectangle Commdn Execution
    /// </summary>
    public class Rect : IExecute
    {
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="canvas"></param>
        public Rect(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Draw Rectangle on Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            int width = Convert.ToInt32(canvas.Parameters[0]);
            int height= Convert.ToInt32(canvas.Parameters[1]);

            canvas.graphics.DrawRectangle(canvas.pen, canvas.xPos - (width / 2), canvas.yPos - (height / 2), width, height);
            if (canvas.Fill.Equals("on"))
            {
                System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(canvas.pen.Color);
                canvas.graphics.FillRectangle(myBrush, canvas.xPos - (width / 2), canvas.yPos - (height / 2), width, height);
            }

        }
    }
}
