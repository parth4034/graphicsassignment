﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for Command Calling and Drawing Shapes on Graphics
    /// </summary>
    public class Command
    {
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="canvas"></param>
        public Command(Canvas canvas)
        {
            ExecuteCommand(canvas);
        }

        /// <summary>
        /// Execute Command
        /// Draw the Shape based on Inputs Command
        /// </summary>
        /// <param name="objCanvas"></param>
        public void ExecuteCommand(Canvas objCanvas)
        {
            if (objCanvas.Command.Equals("clear"))
            {
                Clear objClear = new Clear(objCanvas);
            }
            else if (objCanvas.Command.Equals("moveto"))
            {
                MoveTo objMoveTo = new MoveTo(objCanvas);
            }
            else if (objCanvas.Command.Equals("drawto"))
            {
                DrawTo ObjDrawTo = new DrawTo(objCanvas);
            }
            else if (objCanvas.Command.Equals("rect"))
            {
                Rect ObjRect = new Rect(objCanvas);
            }
            else if (objCanvas.Command.Equals("circle"))
            {
                Circle ObjCircle = new Circle(objCanvas);
            }
            else if (objCanvas.Command.Equals("tri"))
            {
                Triangle ObjTriangle = new Triangle(objCanvas);
            }
        }
    }
}
