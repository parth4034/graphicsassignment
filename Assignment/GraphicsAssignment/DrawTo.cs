﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible For DrawTo Command Execution
    /// </summary>
    public class DrawTo : IExecute
    {

        /// <summary>
        /// Constructor of Class
        /// </summary>
        /// <param name="canvas"></param>
        public DrawTo(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Drawing Line in Graphics
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            canvas.graphics.DrawLine(canvas.pen, canvas.xPos, canvas.yPos, Convert.ToInt32(canvas.Parameters[0]), Convert.ToInt32(canvas.Parameters[1]));
            canvas.xPos = Convert.ToInt32(canvas.Parameters[0]);
            canvas.yPos = Convert.ToInt32(canvas.Parameters[1]);
        }

    }
}
