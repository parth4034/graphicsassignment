﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;



namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for Tri command Execution
    /// </summary>
    public class Triangle : IExecute
    {
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="canvas"></param>
        public Triangle(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Drawing Triangle on Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            Point[] points2 = { new Point(100, 100), new Point(200, 100), new Point(150, 10) };

            SmoothingMode smoothingModeTmp = canvas.graphics.SmoothingMode;
            canvas.graphics.SmoothingMode = SmoothingMode.AntiAlias;
            canvas.graphics.DrawPolygon(canvas.pen, points2);
            canvas.graphics.SmoothingMode = smoothingModeTmp;
        }

    }
}
