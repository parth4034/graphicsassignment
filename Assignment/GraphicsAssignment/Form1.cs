﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    public partial class Form1 : Form
    {

        #region Form Variables

        Bitmap outputBitMap = new Bitmap(530, 440);
        Canvas objCanvas;

        #endregion

        #region Form Init

        /// <summary>
        /// Form object
        /// Load Initial Setting for the Form
        /// Set Canvas Object 
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            objCanvas = new Canvas(Graphics.FromImage(outputBitMap));
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Picture Box Paint Event
        /// used for draw shape on Picture Box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        private void OutputWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.DrawImageUnscaled(outputBitMap, 0, 0);

        }

        /// <summary>
        /// Run Multiline Command Button Click Event
        /// Draw Shapes based on multiple command inputs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRun_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtProgramWindow.Text))
                {
                    HideErrorMessage();
                    //RunMultilineCommand(txtProgramWindow.Lines);
                    ExecuteMultiLineCommand(txtProgramWindow.Lines);
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Check Multiline Command Syntax
        /// Check For Command is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkSyntax_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(txtProgramWindow.Text))
                {
                    HideErrorMessage();
                    IsValidate objIsValid = new IsValidate(txtProgramWindow.Lines);
                    if (!objIsValid.IsValid)
                    {
                        ShowErrorMessage(objIsValid.ErrorMessage);
                    }
                    else
                    {
                        ShowErrorMessage("All the Command are correct.");
                    }

                    //IsValidateMultilineCommand(txtProgramWindow.Lines);
                }
                
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }
        }

        /// <summary>
        /// Single Line Command Click Event
        /// Draw Shapes based on inputs command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(txtcommandLine.Text))
                {
                    //RunLineCommand(txtcommandLine.Text.Trim().ToLower());
                    HideErrorMessage();
                    ExecuteCommand(txtcommandLine.Text.Trim().ToLower());
                }
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }

        }

        #endregion

        #region Methods and Functions

        /// <summary>
        /// Draw Shpes based on given command
        /// commandText - inputs command
        /// </summary>
        /// <param name="commandText"></param>
        public void ExecuteCommand(string CommandText)
        {
            IsValidate objIsValid = new IsValidate(CommandText);

            if (objIsValid.IsValid)
            {
                objCanvas.Command = CommandText.Split(' ')[0];

                if (CommandText.Split(' ').Length > 1)
                    objCanvas.Parameters = CommandText.Split(' ')[1].Split(',');

                if (objCanvas.Command.Equals("reset"))
                {
                    objCanvas.Command = "clear";
                    Command obj = new Command(objCanvas);
                    txtProgramWindow.Text = "";
                }
                else if (objCanvas.Command.Equals("pen"))
                {
                    SetPen(objCanvas.Parameters[0]);
                }
                else if (objCanvas.Command.Equals("fill"))
                {
                    FillOnOff(objCanvas.Parameters[0]);
                }
                else
                {
                    Command objCommand = new Command(objCanvas);
                }

                txtcommandLine.Text = "";
                Refresh();
            }
            else
            {
                ShowErrorMessage(objIsValid.ErrorMessage);
            }
        }

        /// <summary>
        /// Set Pen Color
        /// Execution of Pen Command
        /// </summary>
        /// <param name="color"></param>
        public void SetPen(string color)
        {
            if (color.Equals("green"))
            {
                objCanvas.pen = new Pen(Color.Green, 1);
            }
            else if (color.Equals("red"))
            {
                objCanvas.pen = new Pen(Color.Red, 1);
            }
            else if (color.Equals("blue"))
            {
                objCanvas.pen = new Pen(Color.Blue, 1);
            }
            else if (color.Equals("black"))
            {
                objCanvas.pen = new Pen(Color.Black, 1);
            }
        }

        /// <summary>
        /// Fill Setting - On/Off
        /// Filled Shape with Color
        /// </summary>
        /// <param name="fill"></param>
        public void FillOnOff(string fill)
        {
            if (fill.Equals("on"))
            {
                objCanvas.Fill = "on";
            }
            else if (fill.Equals("off"))
            {
                objCanvas.Fill = "off";
            }
        }


        /// <summary>
        /// Show Error Message on Screen
        /// Message - Validation Message
        /// </summary>
        /// <param name="message"></param>
        public void ShowErrorMessage(string message)
        {
            this.lblErrorMsg.Visible = true;
            this.lblErrorMsg.Text = message;
        }

        /// <summary>
        /// Hide Error Message
        /// </summary>
        public void HideErrorMessage()
        {
            this.lblErrorMsg.Visible = false;
        }

        /// <summary>
        /// Multiline Command Execution
        /// </summary>
        /// <param name="multiCommandText"></param>
        public void ExecuteMultiLineCommand(string[] multiCommandText)
        {
            IsValidate objIsValid = new IsValidate(multiCommandText);

            if (objIsValid.IsValid)
            {
                foreach (string commandText in multiCommandText)
                {
                    ExecuteCommand(commandText.ToString().ToLower());
                }
            }
            else
            {
                ShowErrorMessage(objIsValid.ErrorMessage);
            }
        }

        #endregion

    }
}
