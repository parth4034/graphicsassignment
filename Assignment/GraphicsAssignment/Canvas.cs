﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Canvas Class used to store Shapes setting data
    /// Canvas contain information of Drawing object
    /// </summary>
    public class Canvas
    {

        #region Property & Variables

        /// <summary>
        /// XPosition and Yposition variable to store position of X and Y axis
        /// </summary>
        public int xPos { get; set; }
        public int yPos { get; set; }

        /// <summary>
        /// Graphis variable to store Gaphics shapes
        /// </summary>
        public Graphics graphics { get; set; }

        /// <summary>
        ///  Pen property
        ///  seting Different Pen based on command
        /// </summary>
        public Pen pen { get; set; }


        /// <summary>
        /// Fill property to used Fill Shapes setting ON and OFF
        /// </summary>
        public string Fill { get; set; } = "off";

        /// <summary>
        /// Contain Command Information
        /// </summary>
        public string Command { get; set; }

        /// <summary>
        /// Contain Command Parameter
        /// </summary>
        public string[] Parameters { get; set; }

        #endregion


        #region Constructor

        /// <summary>
        /// Canvas Constructor
        /// Default Settings for Canvas
        /// </summary>
        /// <param name="g"></param>
        public Canvas(Graphics graphics)
        {
            this.graphics = graphics;
            xPos = yPos = 20;
            this.pen = new Pen(Color.Black, 1);

            graphics.DrawRectangle(new Pen(Color.Red, 1), xPos, yPos, 2, 2);
        }

        public Canvas()
        { 

        }

        #endregion


       
        
    }
}
