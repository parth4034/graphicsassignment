﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace GraphicsAssignment
{
    /// <summary>
    /// Responsible for MoveTo Command Execution
    /// </summary>
    public class MoveTo : IExecute
    {
        /// <summary>
        /// Constructor of Class
        /// </summary>
        /// <param name="canvas"></param>
        public MoveTo(Canvas canvas)
        {
            this.Execute(canvas);
        }

        /// <summary>
        /// Moved Pointer in Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void Execute(Canvas canvas)
        {
            canvas.graphics.DrawRectangle(new Pen(Color.Red, 1), Convert.ToInt32(canvas.Parameters[0]), Convert.ToInt32(canvas.Parameters[1]), 2, 2);
            canvas.xPos = Convert.ToInt32(canvas.Parameters[0]);
            canvas.yPos = Convert.ToInt32(canvas.Parameters[1]);
        }
    }
}
