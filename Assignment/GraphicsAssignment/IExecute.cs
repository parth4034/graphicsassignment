﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsAssignment
{
    /// <summary>
    /// Interface of Command Execution Method
    /// </summary>
    public interface IExecute
    {
        void Execute(Canvas obj);
        
    }
}
